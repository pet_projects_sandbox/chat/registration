package com.test.pet
import com.typesafe.config.ConfigFactory

object Main extends App {
  val conf = ConfigFactory.systemEnvironment()
  println(conf.getString("REG_SERVICE_VERSION"))
}
