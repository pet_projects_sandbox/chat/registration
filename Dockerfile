FROM openjdk:8-jre-alpine

ARG version

ENV REG_SERVICE_VERSION=$version

RUN mkdir -p /opt/app
WORKDIR /opt/app

COPY ./run.sh ./target/scala-2.12/reg-service.jar ./
ENTRYPOINT ["java", "-jar", "reg-service.jar"]