name := "registration"

version := "0.1"

scalaVersion := "2.12.8"

mainClass in assembly := Some("com.test.pet.Main")
assemblyJarName in assembly := "reg-service.jar"

libraryDependencies += "com.typesafe.slick" %% "slick" % "3.3.2"